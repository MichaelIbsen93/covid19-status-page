const apiUrl = 'https://covid19.mathdro.id/api/';

const months = [
    'Januar',
    'Februar',
    'Marts',
    'April',
    'Maj',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'December'
]

// output fields.
const recovered = document.getElementById('recovered');
const confirmed = document.getElementById('confirmed');
const deaths = document.getElementById('deaths');
const infected = document.getElementById('infected');
const countries = document.getElementById('countries');
const lastUpdated = document.getElementById('last-updated');
const country = document.getElementById('country');


countries.addEventListener('input', (e) => {
    const countryCode = e.target.value;

    updateStatus(countryCode);
});

function updateStatus(countryCode) {
    loading();
    fetch(apiUrl + 'countries/' + countryCode)
        .then(resp => resp.json())
        .then(resp => {

            initializeCountUp(resp);

            let date = new Date(resp?.lastUpdate);

            const day = date.getDate();
            const month = months[date.getMonth()];
            const year = date.getFullYear();
            const hour = date.getHours();
            const minutes = date.getMinutes();

            const dateString = `D. ${day}/${month}/${year} - ${hour}:${minutes}`;

            lastUpdated.innerHTML = dateString;
        }).catch(err => {
            const errorMsg = 'N/A';

            recovered.innerHTML = errorMsg;
            confirmed.innerHTML = errorMsg;
            deaths.innerHTML = errorMsg;
            lastUpdated.innerHTML = errorMsg;
            infected.innerHTML = errorMsg;
        });
}

function loading() {
    const loadMsg = `<div class="spinner-border" role="status">
    <span class="sr-only">Loading...</span>
        </div>`;

    recovered.innerHTML = loadMsg;
    confirmed.innerHTML = loadMsg;
    deaths.innerHTML = loadMsg;
    lastUpdated.innerHTML = loadMsg;
    infected.innerHTML = loadMsg;
}

/**
 * Update numbers with a animation.
 * 
 * @param {*} resp 
 */
function initializeCountUp(resp) {
    let recoveredCountUp = new CountUp('recovered', resp?.recovered.value);
    if (!recoveredCountUp.error) {
        recoveredCountUp.start();
    } else {
        console.error(recoveredCountUp.error);
    }

    let confirmedCountUp = new CountUp('confirmed', resp?.confirmed.value);
    if (!confirmedCountUp.error) {
        confirmedCountUp.start();
    } else {
        console.error(confirmedCountUp.error);
    }

    let deathsCountUp = new CountUp('deaths', resp?.deaths.value);
    if (!deathsCountUp.error) {
        deathsCountUp.start();
    } else {
        console.error(deathsCountUp.error);
    }

    let infectedCountUp = new CountUp('infected', resp?.confirmed.value - (resp?.recovered.value + resp?.deaths.value));
    if (!infectedCountUp.error) {
        infectedCountUp.start();
    } else {
        console.error(infectedCountUp.error);
    }
}

/**
 * Load countries into dropdown menu.
 */
function loadCountries() {
    fetch(apiUrl + 'countries')
        .then(resp => resp.json())
        .then(resp => {
            resp.countries.forEach(country => {

                if (country.iso2 === 'DK') {
                    countries.innerHTML += `<option value="${country.iso2}" selected>(Default) ${country.name}</option>`;
                    updateStatus(country.iso2);
                    return;
                }

                countries.innerHTML += `<option value="${country.iso2}">${country.name}</option>`;
            });
        });
}

/**
 * Example: Convert 10000 to 10.000.
 * 
 * @param {int} x
 */
function thousandSeparator(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

// initial data.
loadCountries();